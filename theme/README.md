# startbootstrap creative theme simplified

Just some base styles nothing special. No js bundling for now.

```` bash
npm install

# compile once
npm run sass

# watch
npm start

# use dist/bootstrap.min.css
````

## Attribution and License

The original repo and author is [https://github.com/BlackrockDigital/startbootstrap-creative](David Miller), everything is published under [https://github.com/BlackrockDigital/startbootstrap-creative/blob/gh-pages/LICENSE](MIT License).
