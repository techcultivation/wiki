# Ikistrap, a Bootstrap 4 theme for ikiwiki

[Ikiwiki](https://ikiwiki.info/) is a very powerful [wiki](https://en.wikipedia.org/wiki/Wiki) compiler.
However, its default theme is very minimalistic.
Ikistrap is a theme that uses [Bootstrap 4](http://v4-alpha.getbootstrap.com/) to ensure you have a wiki that looks professional,
whether you are viewing it on your desktop computer or on your mobile phone.

To use ikistrap in your own wiki, just add the following to your setup file:

    templatedir: /path/to/ikistrap/templates
    underlaydir: /path/to/ikistrap/basewiki

Ikistrap comes with an example wiki that shows off its features,
and shows you how to integrate some Bootstrap 4 features into your `.mdwn` files.
Use the `Makefile` to compile the example wiki.

## Setup
```` bash
#install dependencies
sudo apt install ikiwiki nginx fcgiwrap build-essential git

# copy nginx.conf

# create ikiwiki user
sudo adduser ikiwiki # use "ubuntu" in vagrant

# edit fcgiwrap config to use ikiwiki user
sudo vim /lib/systemd/system/fcgiwrap.service

# install cpan dependency
sudo cpan -T Text::MultiMarkdown Mail::Sendmail CGI::Session

# generate ssh pub key and add as deploy key with write access on the git remote
sudo su ikiwiki
ssh-keygen

# generate output folder and cgi wrapper
ikiwiki --setup ikiwiki.setup

# set the webhook url to https://techcultivation.org/wiki/ikiwiki.cgi?do=ping

# open http://localhost:8080/wiki/
````
